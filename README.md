Library gallay assignment

### Overview ###

* backend server developed using Java, Hibernate and H2 memory db
* UI using react native

### Backend setup ###

* git clone {url}
* CMD go to backend folder. and run spring application


### UI setup ###

* CMD to UI folder
* npm install
* npm start

### Rest API ###

Create Library and Books. It Accept list of library with or without books.
POST: http://localhost:8080/api/library
Pyload:
[
  {
    "name": "British Library",
    "books": [
      {
        "bookName": "Java certification",
        "authorName": "Kethy  Seriya"
      },
      {
        "bookName": "Go Lang",
        "authorName": "XYZ  author"
      }
    ]
  },
  {
    "name": "New York Public Library",
    "books": [
      {
        "bookName": "AI & ML",
        "authorName": "author1"
      },
      {
        "bookName": "Business Development",
        "authorName": "XYZ  author"
      }
    ]
  }
]


GET: http://localhost:8080/api/library

Give list of library

response:
[
    {
        "name": "British Library",
        "id": 1
    },
    {
        "name": "New York Public Library",
        "id": 2
    }
]


GET: http://localhost:8080/api/library/books/1

Gives list of books associated with library by library Id.

response:
[
    {
        "id": 1,
        "bookName": "Java",
        "authorName": "Kethy seria"
    },
    {
        "id": 2,
        "bookName": "Python",
        "authorName": "Nihar"
    },
    {
        "id": 3,
        "bookName": "Go Lang",
        "authorName": "Nihar"
    }
]

PUT: http://localhost:8080/api/book/1
Pyload:
{
  "id": 1,
  "bookName": "Java certification1",
  "authorName": "Kethy Serya1"
}

PUT API to update book detais;
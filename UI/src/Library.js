import React, {Component} from 'react';
import Panel from 'react-bootstrap/lib/Panel'
import Button from 'react-bootstrap/lib/Button'
import BookDetails from './BookDetails'
import App from './App'
import axios from 'axios'

export default class Library extends Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  //function which is called the first time the component loads
  componentDidMount() {
    this.getLibraryData();
  }

  //Function to get the Library Data from json

  getLibraryData() {
    console.log('calling');
    axios.get('http://localhost:8080/api/library').then(response => {
      console.log(response.data);
      this.setState({libraryList: response})
    })
  };

  render() {
    if (!this.state.libraryList)
      return (<p>Data not available</p>)
    return (<div className="addmargin">
      <div className="col-md-3">
        {

          this.state.libraryList.data.map(library => <Panel bsStyle="info" key={library.name} className="centeralign">
            <Panel.Heading>
              <Panel.Title componentClass="h3">{library.name}</Panel.Title>
            </Panel.Heading>
            <Panel.Body>
              <Button bsStyle="info" onClick={() => this.setState({selectedLibrary: library.id})}>

                View Book Details

              </Button>

            </Panel.Body>
          </Panel>)
        }
      </div>
      <div className="col-md-6">
        <BookDetails val={this.state.selectedLibrary}/>
        
      </div>
    </div>)
  }

}

import React, {Component} from 'react';
import Panel from 'react-bootstrap/lib/Panel'
import axios from 'axios'


//This Component is a child Component of Customers Component
export default class BookDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {}
  }

  //Function which is called when the component loads for the first time
  componentDidMount() {
    this.getBookDetails(this.props.val)
  }

  //Function which is called whenver the component is updated
  componentDidUpdate(prevProps) {

    //get Book Details only if props has changed
    if (this.props.val !== prevProps.val) {
      this.getBookDetails(this.props.val)
    }
  }

  //Function to Load the bookdetails data from json.
  getBookDetails(id) {
    axios.get('http://localhost:8080/api/library/books/' + id).then(response => {
      this.setState({bookDetails: response})
      console.log('response:', response.data);
    })
  };

  render() {
    if (!this.state.bookDetails)
      return (<p></p>)
    return (<div className="addmargin">
      
    <div className="rows">
      {


        this.state.bookDetails.data.map(book => <Panel bsStyle="info" key={book.bookName} className="centeralign">
          <Panel.Heading>
            <Panel.Title componentClass="h3">{book.bookName}</Panel.Title>
          </Panel.Heading>
          <Panel.Body>
          <p>Book Name : {book.bookName}</p>
          <p>Author Name : {book.authorName}</p>

          </Panel.Body>
        </Panel>)
      }
    </div>
    
  </div>)
  }
}

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Library from './Library'
import { BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';


class App extends Component {
  render() {
    console.log("Host URL"+process.env.PUBLIC_URL);
    return (

      <Router basename={process.env.PUBLIC_URL}>
        <div className="App">
        <header className="App-header">
          <h1 className="App-title">Library Books Gallary</h1>
        </header>
          <Switch>
                <Route exact path= "/" render={() => (
                  <Redirect to="/librarylist"/>
                )}/>
                 <Route exact path='/librarylist' component={Library} />
          </Switch>
      </div>
    </Router>
    );
  }
}

export default App;

package com.assignment.library.testdata;

import com.assignment.library.entity.Book;
import com.assignment.library.entity.Library;
import com.assignment.library.service.LibraryService;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class TestData {

    @Autowired
    private LibraryService libraryService;
    @EventListener
    public void appReady(ApplicationReadyEvent event) {

        libraryService.createLibraries(loadData());;
    }

    private List<Library> loadData() {
        List<Library> libraries = new ArrayList<>();

        Library lib1 = new Library();
        lib1.setName("British Library");
        List<Book> list1 = new ArrayList<>();
        Book b11 = new Book();
        b11.setBookName("Java Certification");
        b11.setAuthorName("Kethy seyria");;
        list1.add(b11);
        Book b12 = new Book();
        b12.setBookName("Python");
        b12.setAuthorName("Nihar");
        list1.add(b12);
        Book b13 = new Book();
        b13.setBookName("Go Lang");
        b13.setAuthorName("Nihar");
        list1.add(b13);
        Book b14 = new Book();
        b14.setBookName("Java");
        b14.setAuthorName("Kethy seria");;
        list1.add(b14);
        Book b15 = new Book();
        b15.setBookName("React Js");
        b15.setAuthorName("google");
        list1.add(b15);
        Book b16 = new Book();
        b16.setBookName("java script");
        b16.setAuthorName("google inc");
        list1.add(b16);
        lib1.setBooks(list1);
        libraries.add(lib1);

        Library lib2 = new Library();
        lib2.setName("Library and Archives Canada");;
        List<Book> list2 = new ArrayList<>();
        Book b21 = new Book();
        b21.setBookName("Compiler design");
        b21.setAuthorName("Ramamurthy K");;
        list2.add(b21);
        Book b22 = new Book();
        b22.setBookName("Spring with Hibernate");
        b22.setAuthorName("Santosh K");
        list2.add(b22);
        Book b23 = new Book();
        b23.setBookName("Hacking in nutshell");
        b23.setAuthorName("Nihar");
        list2.add(b23);
        lib2.setBooks(list2);
        libraries.add(lib2);


        Library lib3 = new Library();
        lib3.setName("Library and Archives Canada");;
        List<Book> list3 = new ArrayList<>();
        Book b31 = new Book();
        b31.setBookName("Business development");
        b31.setAuthorName("steve John");;
        list3.add(b31);
        Book b32 = new Book();
        b32.setBookName("AL ML");
        b32.setAuthorName("unknown");
        list3.add(b32);
        Book b33 = new Book();
        b33.setBookName("Data processing");
        b33.setAuthorName("Carls");
        list3.add(b33);
        lib3.setBooks(list3);
        libraries.add(lib3);

        return libraries;
    }
}

package com.assignment.library.service;

import com.assignment.library.entity.Book;

public interface BookService {

    Book getBookById(Long bookId);
    void updateBook(Book book);
}

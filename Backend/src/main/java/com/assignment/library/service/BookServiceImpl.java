package com.assignment.library.service;

import com.assignment.library.entity.Book;
import com.assignment.library.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService{

    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Book getBookById(Long bookId) {
        return bookRepository.getOne(bookId);
    }

    @Override
    public void updateBook(Book book) {
        bookRepository.save(book);
    }
}

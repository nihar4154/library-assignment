package com.assignment.library.service;

import com.assignment.library.entity.Book;
import com.assignment.library.entity.Library;
import com.assignment.library.repository.LibraryRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LibraryServiceImpl implements LibraryService {

    private final LibraryRepository libraryRepository;

    public LibraryServiceImpl(LibraryRepository libraryRepository) {
        this.libraryRepository = libraryRepository;
    }

    @Override
    public void createLibraries(List<Library> libraryList) {
        libraryRepository.saveAll(libraryList);
        //libraryList.forEach(l -> libraryRepository.save(l));
    }

    @Override
    public List<Library> getLibrary() {
        return libraryRepository.findAll();
    }

    @Override
    public List<Book> getLibraryBooks(long id) {

        Optional<Library>  l = libraryRepository.findById(id);
        return l.map(Library::getBooks).orElse(null);

    }
}

package com.assignment.library.service;

import com.assignment.library.entity.Book;
import com.assignment.library.entity.Library;

import java.util.List;
import java.util.Optional;

public interface LibraryService {

    void createLibraries(List<Library> libraryList);
    List<Library> getLibrary();

    List<Book> getLibraryBooks(long id);
}

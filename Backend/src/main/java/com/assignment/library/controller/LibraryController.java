package com.assignment.library.controller;
import com.assignment.library.entity.Book;
import com.assignment.library.entity.Library;
import com.assignment.library.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LibraryController {

    @Autowired
    private LibraryService libraryService;

    @PostMapping("/api/library")
    public void createLibrary(@RequestBody  List<Library> libraryList){
        libraryService.createLibraries(libraryList);
        System.out.println("Libraries Saved Successfully");
    }

    @GetMapping("/api/library")
    public List<Map> getLibrary() {
        List<Map> list = new ArrayList<>();
        for(Library l : libraryService.getLibrary()) {
            Map m = new HashMap();
            m.put("id", l.getId());
            m.put("name", l.getName());
            list.add(m);
        }
        return list;
    }

    @GetMapping("/api/library/books/{libraryId}")
    public List<Book> getBooks(@PathVariable(name="libraryId")Long libraryId) {
         return libraryService.getLibraryBooks(libraryId);
    }
}

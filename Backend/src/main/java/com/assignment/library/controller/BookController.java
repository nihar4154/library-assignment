package com.assignment.library.controller;

import com.assignment.library.entity.Book;
import com.assignment.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BookController {

    @Autowired
    private BookService bookService;

    @PutMapping("/api/book/{bookId}")
    public void updateBook(@RequestBody Book book,
                           @PathVariable(name="bookId")Long bookId){
        Book res = bookService.getBookById(bookId);
        if(res != null){
            bookService.updateBook(book);
        }

    }
}

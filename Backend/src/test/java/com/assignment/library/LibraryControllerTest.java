package com.assignment.library;

import com.assignment.library.entity.Library;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = LibraryApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LibraryControllerTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void addLibraryAndBooks() {
     //   List<Library> data = loadData();
    }

    private List<Library> loadData() {
        ObjectMapper mapper = new ObjectMapper();
        List<Library> libraries = new ArrayList<>();
        Library library1 = null;
        try {
            library1 =  mapper.readValue(new File("testdata.json"), Library.class);
            libraries.add(library1);
        } catch (JsonGenerationException | JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return libraries;
    }
}
